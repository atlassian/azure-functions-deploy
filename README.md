# Bitbucket Pipelines Pipe: Azure Functions Deploy

A pipe that uses the Azure CLI to deploy a zipped code package to [Azure Functions][Azure Functions].

Note: This pipe was forked from https://bitbucket.org/microsoft/azure-functions-deploy for future maintenance purposes.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/azure-functions-deploy:2.2.1
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      FUNCTION_APP_NAME: '<string>'
      ZIP_FILE: '<string>'
      # DEBUG: '<boolean>' # Optional
```


## Variables

| Variable               | Usage                                                       |
| ---------------------- | ----------------------------------------------------------- |
| AZURE_APP_ID (*)       | The app ID, URL or name associated with the service principal required for login. |
| AZURE_PASSWORD (*)     | Credentials like the service principal password, or path to certificate required for login. |
| AZURE_TENANT_ID  (*)   | The AAD tenant required for login with service principal. |
| AZURE_AKS_NAME (*)     | Name of the AKS management service to connect to.
| FUNCTION_APP_NAME (*)  | Name of the Function app that code should be deployed to.  |
| ZIP_FILE (*)           | Path of the package that should deployed to the Functions app.  |
| DEBUG                  | Turn on extra debug information. Default: `false`. |

_(*) = required variable._


## Prerequisites

### Service principal

You will need a service principal with sufficient access to create an Azure Functions app, or update an existing Functions app. To create a service principal using the Azure CLI, execute the following command in a bash shell:

```sh
az ad sp create-for-rbac --name MyServicePrincipal
```

Refer to the following documentation for more detail:

* [Create an Azure service principal with Azure CLI][Create an Azure service principal with Azure CLI]


### Azure Functions app

Using the service principal credentials obtained in the previous step, you can use the following commands to create an Azure Functions instance in a bash shell:

```bash
az login --service-principal --username ${AZURE_APP_ID}  --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}

az group create --name ${AZURE_RESOURCE_GROUP} --location australiaeast

az storage account create --name ${FUNCTION_APP_STORAGE_NAME} --location australiaeast --resource-group ${AZURE_RESOURCE_GROUP} --sku Standard_LRS

az functionapp create --name ${FUNCTION_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP} --storage-account ${FUNCTION_APP_STORAGE_NAME}
```

Refer to the following documentation for more detail:

* [Create your first function from the command line][Create your first function from the command line]


## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/azure-functions-deploy:2.2.1
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      FUNCTION_APP_NAME: 'my-function'
      ZIP_FILE: 'application.zip'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2021 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[Azure Functions]: https://docs.microsoft.com/en-us/azure/azure-functions/functions-overview
[Create an Azure service principal with Azure CLI]: https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli
[Create your first function from the command line]: https://docs.microsoft.com/en-us/azure/azure-functions/functions-create-first-azure-function-azure-cli
