# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.2.1

- patch: Internal maintenance: Bump base docker image to 2.70.0.
- patch: Internal maintenance: Update pipes versions in pipelines configuration file.

## 2.2.0

- minor: Bump base docker image to version 2.67.
- patch: Internal maintenance: bump project's dependencies to the latest.

## 2.1.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 2.1.0

- minor: Internal maintenance: Update pipe's docker image to mcr.microsoft.com/azure-cli:2.58.0.
- patch: Internal maintainance: Fix dependencies.
- patch: Internal maintenance: Bump project's dependencies to the latest.

## 2.0.0

- major: Internal maintenance: Update pipe's docker image to mcr.microsoft.com/azure-cli:2.21.0.

## 1.0.1

- patch: Internal maintenance: make pipe structure consistent to others.

## 1.0.0

- major: Note: This pipe was forked from https://bitbucket.org/microsoft/azure-functions-deploy for future maintenance purposes.
